// console.log("Hewwo~!!");
console.log("ES6 Updates:");


/*
	ES6 Update
	- ECMAScript(2015)
	- ECMAScript - standard used to create implementation of language, one of which is JavaScript
	- brings new features to JavaScript
*/

// [SECTION] Exponent Operator

	console.log("Exponent Operator:")

	console.log("//before ES6")
	// before ES6
		const firstNum = 8 ** 2;
		console.log(firstNum);

	console.log("//after ES6")
	// ES6
		const secondNum = Math.pow(8,2);
		// Math.pow(base,exponent);


// [SECTION] Template Literals
	// allows us to write strings without using concatenation operator(+);
	// greatly helps with code readability.

	console.log("Template Literals:")
	let name = "John";

	console.log("//before ES6")
		// before ES6
			let message = "Hello " + name + "! Welcome to promramming."
			console.log(message);

	console.log("//after ES6")
		// ES6
		// uses backticks(``)

			message = `Hello ${name}! 
			Welcome to programming.`
			console.log(message); //reflects with indents

	// allows us to perform operations
		const interestRate = 0.1;
		const principal = 1000;
		console.log(`The interest on your savings account is : ${interestRate*principal}`);

// [SECTION] Array Destructuring
	// allows us to unpack elements in an array into distinct variables
	// allows us to name array elements with variables instead of using index.
/*
	SYNTAX:
		let/const [variableA, variableB, . . .] = arrayName;
*/
	console.log("Array Destructuring:")

	const fullName = ["Juan","Dela","Cruz"];
	console.log("//before ES6")
	// before ES6
		let firstName = fullName[0];
		let middleName = fullName[1];
		let lastName = fullName[2];
		console.log(`Hello ${firstName} ${middleName} ${lastName}`);
	console.log("//after ES6")
	// ES6
		const [fName, mName, lName] = fullName;

		console.log(fName);
		console.log(mName);
		console.log(lName);

			//Mini-activity
				//Array destructuring
				let array = [1, 2, 3, 4, 5];

					//destructure the array into 5 different variables;

				let [a,b,c,d,e] = array;
				console.log(a);
				console.log(b);
				console.log(c);
				console.log(d);
				console.log(e);

// [SECTION] Object Destructuring
	// allows us to unpack properties of ebjects into distinct variables.
/*
	SYNTAX:
		let/cosnt {propertyA,propertyB, . . .} = objectName;
*/
	console.log("Object Destructuring:")
	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	}
	console.log("//before ES6")
	// before ES6
		let gName = person.givenName;
		let midName = person.maidenName;
		let famName = person.familyName;
		console.log(gName);
		console.log(midName);
		console.log(famName);
	console.log("//after ES6")
	// ES6
		let { givenName, maidenName, familyName } = person;
		console.log(givenName);
		console.log(maidenName);
		console.log(familyName);

// [SECTION] Arrow Functions
	// compact alternative syntax to traditional functions

	const hello = () =>{
		console.log("Hello World!");
	}

	hello();
	/*
	// function expression
		const hello = function() =>{
			console.log("Hello World!");
		}

	// function declaration
		function hello() =>{
			console.log("Hello World!");
		}
	*/

// [SECTION] Implicit Return
	// there are instances when you can omit return statement
	// thsi works because even withour using return keyword

	const add = ( x , y ) => x + y

	// const add = ( x , y ) => {
	// 	return x + y
	// }

	console.log("Implicit Return:")
	console.log(add(1,2));

	const subtract = ( x , y ) => {
		return x - y
	}
	console.log(subtract(10,5));

// [SECTION] Default Function Argument Value

	// const greet = (firstName, lastName) => {
	// 	return `Good Afternoon, ${firstName} ${lastName}!`;
	// }
	// console.log(greet()); // undefined, undefined
	// console.log(greet("Chris")); // Chris, undefined

	const greet = (firstName = "firstName", lastName = "lastName") => {
		return `Good Afternoon, ${firstName} ${lastName}!`;
	}
	console.log(greet()); // with placeholder
	console.log(greet("Chris")); // with placeholder

	// function greet(firstName, lastName){
	// 	return `Good Afternoon, ${firstName} ${lastName}!`;
	// }
	// console.log(greet());
	// console.log(greet("Chris")); 

// [SECTION] Class-based Object Blueprints
	// allows us to create/instantiation of objects using classes blueprints

	// create class
		// consdtructor is a special method of class for creating/initializing an object of the class
	/*
		SYNTAX:
			class className{
				constructor(objectValueA){
					this.objectPropertyA = objectValueA,
					this.objectPropertyB = objectValueB,
					. . .
				}
			}
	*/

	class Car{
		constructor(brand, name, year){
			this.carBrand = brand,
			this.carName = name,
			this.carYear = year
		}
	}

	let car = new Car("Toyota", "Hilux-Pickup", 2015);
	console.log(car);

	car.carBrand = "Nissan"
	console.log(car);


	